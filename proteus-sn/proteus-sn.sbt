<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">
  <Includes>
    <File>../neams/internal/templates/mcc3.sbt</File>
    <File>internal/templates/driver_input.sbt</File>
  </Includes>

  <Views>
    <View Type="Group" Name="Proteus-SN" Label="Proteus-SN" TopLevel="true"
      TabPosition="North" FilterByCategory="false">
      <Views>
        <View Title="MCC3" />
        <View Title="Driver input" />
      </Views>
    </View>

    <View Type="Instanced" Title="MCC3" Label="MCC3">
      <InstancedAttributes>
        <Att Name="mcc3-instance" Type="mcc3" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Driver input" Label="Driver input">
      <InstancedAttributes>
        <Att Name="driver_input-instance" Type="driver_input" />
      </InstancedAttributes>
    </View>

  </Views>
</SMTK_AttributeResource>
